//
//  AppDelegate.swift
//  iWeather
//
//  Created by Lars Klingsten on 26/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        "application launched (updating forecasts/Load UserDefaults)".ToConsole()
        LoadSaveUserDefaults().loadUserDefaults()
        Data.getInstance.UpdatAllForecasts ()
        return true
    }
    
    // Called when the application is about to terminate. Saves data.
    func applicationWillTerminate(application: UIApplication) {
        "application terminated (Save UserDefaults)".ToConsole()
        LoadSaveUserDefaults().saveUserDefaults()
    }
}

