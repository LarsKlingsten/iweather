//
//  CellMasterWeatherDetails.swift
//  iWeather
//
//  Created by Lars Klingsten on 27/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import UIKit


class CellMasterWeatherDetails: UITableViewCell {
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var temperatur: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
}