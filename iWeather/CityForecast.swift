//
//  Forecast.swift
//  iWeather
//
//  Created by Lars Klingsten on 26/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import Foundation

struct CityForecast {
    
    var country:         String
    var city   :         String
    var weather:         String
    var tempC:           Double
    var feelsC:          Double
    var updatedWeather:  String
    var updatedDownload: NSDate
    var isDataAvailable: Bool
    var latitude       : String
    var longitude      : String
    
}