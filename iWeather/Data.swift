//
//  Data.swift
//  iWeather
//
//  Created by Lars Klingsten on 27/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import Foundation



class Data {
  
    // singleton
    static let getInstance: Data = Data() // singleton swift 1.2 version
    private init() { }
    
    var cityForecasts = [CityForecast] ()
    
    // update forecast
    func updateCityForeCast (cityForecast: CityForecast) {
        if let index = getCityForeCastIndex(cityForecast)   {
            cityForecasts[index] = cityForecast
        }
    }
    
    // find forecast (by index) or return nil if not found
    func getCityForeCastIndex(cityForecast: CityForecast) -> Int? {
        for i in 0..<cityForecasts.count {
            if (cityForecasts[i].city == cityForecast.city && cityForecasts[i].country == cityForecast.country)  {
                return i
            }
        }
        return nil
    }
    
    // remove forecast
    func removeCityForecast (cityForecast: CityForecast) {
        if let index = getCityForeCastIndex( cityForecast ) {
            cityForecasts.removeAtIndex(index)
        }
    }
    
    // add entry - if an existing one does not exists
    func AddEntry (cityForecast: CityForecast ) {
        if getCityForeCastIndex(cityForecast) == nil  {
            cityForecasts.append(cityForecast)
        }
    }
    
    // update all
    func UpdatAllForecasts () {
        "Data Class: start -> updating all Forecasts".ToConsole()
        let startTime = CFAbsoluteTimeGetCurrent() // start timer:
        for i in 0 ..< self.cityForecasts.count {
            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
              dispatch_async(backgroundQueue, {
                let myJson = Json();
                self.cityForecasts[i] = myJson.GetForecast(self.cityForecasts[i].latitude, longitude: self.cityForecasts[i].longitude)
                "Data Class: working on fetch json data for index=\(i)".ToConsole()
             })
        }
        let timeElapsed =  CFAbsoluteTimeGetCurrent() - startTime
        
        NSString(format: "Data Class: compl -> upda Function call comleted  in %.6f sec", timeElapsed).description.ToConsole()
    }
}