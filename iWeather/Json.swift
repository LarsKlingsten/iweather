//
//  Json.swift
//  iWeather
//
//  Created by Lars Klingsten on 26/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//
//  note to self : use http://marianoguerra.github.io/json.human.js/  -> tranlate Json for hummans
//  note to self : wunderground ID: a4ba1a5e174412f5 
//  note to self : LoginID: junk@ocpl.com.sg 

import Foundation

class Json {
    func GetForecast(latitude: String, longitude: String) -> CityForecast {
        
        let url = NSURL(string: "http://api.wunderground.com/api/a4ba1a5e174412f5/conditions/q/\(latitude),\(longitude).json")
        
        let request = NSURLRequest(URL: url!)
        
        if let connectionData = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil) {
            let rawData = JSON(data: connectionData )
            let entry = rawData["current_observation"]
            
            let country   = entry["display_location"]["country_iso3166"].stringValue
            let city      = entry["display_location"]["city"].stringValue
            
            let weather = entry["weather"].stringValue
            let tempC   = entry["temp_c"].stringValue.StringToDouble()
            let feelsC  = entry["feelslike_c"].stringValue.StringToDouble()
            let updatedWeather = entry["observation_time_rfc822"].stringValue
            let updatedDownload =   NSDate()
            let isDataAvailable: Bool = city.isEmpty ? false : true
            
            let cityForecast = CityForecast (
                country        : country,
                city           : city,
                weather        : weather,
                tempC          : tempC,
                feelsC         : feelsC,
                updatedWeather : updatedWeather,
                updatedDownload: updatedDownload,
                isDataAvailable: isDataAvailable,
                latitude       : latitude,
                longitude      : longitude
            )
            return cityForecast // all good
        }
                
        // failed to load
        return CityForecast (country: "n/a", city: "n/a", weather: "failed", tempC: -9999, feelsC: -9999,
            updatedWeather: "n/a", updatedDownload:NSDate() ,isDataAvailable: false,
            latitude : latitude, longitude : longitude)
    }
}