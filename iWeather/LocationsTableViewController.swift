//
//  LocationsTableViewController.swift
//  iWeather
//
//  Created by Lars Klingsten on 27/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import UIKit
import Foundation

class LocationsTableViewController: UITableViewController {
    
    let myData = Data.getInstance
    var updateNow = [String: Bool]() // keep track of the table-rows that need to updated (when user clicks "update")
    let degC = "\u{00B0}C"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // do not reply on Apple to set the height
        self.tableView.rowHeight = 55.0
        
        // create the 'edit' button, and attach it to a function
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        // create the 'update' button, and attach it to function/action
        let btnUpdate =  UIBarButtonItem(title: "Update", style: .Plain, target: self, action: "updateJsonCityforecasts")
        self.navigationItem.rightBarButtonItem = btnUpdate
    }
    
    //  "update" clicked
    func updateJsonCityforecasts () {
        
        // Only update if data exist, and last update was more than 30 seconds ago. (and don't tell the user)
        if myData.cityForecasts.count > 0  {
            let myEntry = myData.cityForecasts[0]
            let elapsedTime = NSDate().timeIntervalSinceDate(myEntry.updatedDownload)
            if elapsedTime > 30 {
                
                // setup each of the forecast to true, meaning that they should be updated (done later circa line 90)
                for i in 0 ..< myData.cityForecasts.count {
                    updateNow[myData.cityForecasts[i].country+myData.cityForecasts[i].city] = true
                }
            }
            else {
                "Your data was updated just \(elapsedTime) seconds ago. I'm not going to refresh it again.".ToConsole()
            }
        }
        
        // trigger the update the table (sometimes a refresh is good)
        self.tableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myData.cityForecasts.count
    }
    
    // called when a row deletion action is confirmed
    override func tableView(tableView: UITableView,
        commitEditingStyle editingStyle: UITableViewCellEditingStyle,
        forRowAtIndexPath indexPath: NSIndexPath) {
            
            switch editingStyle {
            case .Delete:
                let cityForecast = myData.cityForecasts[indexPath.row]
                "deleting \(cityForecast.city) @row \(indexPath.row)".ToConsole()
                myData.removeCityForecast(cityForecast)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
                // Hack - required on the simulator to ensure that user defaults are saved.
                LoadSaveUserDefaults().saveUserDefaults()
            default:
                return
            }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! CellMasterWeatherDetails
        
        let entry = myData.cityForecasts[indexPath.row]
        
        // "update" triggered by user
        if updateNow[entry.country + entry.city] == true {
            updateNow[entry.country + entry.city] = false
            "update Entry now \(entry.country + entry.city)".ToConsole()
            
            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
            
            // start background queue
            dispatch_async(backgroundQueue, {
                
                // download the from API
                let testCityForecast = Json().GetForecast(entry.latitude, longitude: entry.longitude)
                
                // test if the data retriaval was  success only the update
                if testCityForecast.isDataAvailable == true {
                    self.myData.cityForecasts[indexPath.row] = testCityForecast
                }
                else {
                    "failed to download. Communication errors with the API".ToConsole()
                }
                
                //  main queue (needed for GUI updates)  -> runs only after background queue has completed
                dispatch_async(dispatch_get_main_queue(), {
                    
                    let updatedEntry = self.myData.cityForecasts[indexPath.row]
                    
                    // This check need if user has scrolled the row out of display (as rows are being reused, and may have changed)
                   //  if  (updatedEntry.country == cell.country.text && updatedEntry.city == cell.city.text) {
                        cell.weather.text = updatedEntry.weather
                        cell.temperatur.text   =  updatedEntry.tempC.description + self.degC
                       ("updated cell: \(updatedEntry.city) \(updatedEntry.updatedDownload) < - old \(entry.updatedDownload)").ToConsole()
                   //  }
                   //  else {
                   //      ("Skipped. Updated City \(updatedEntry.city) does not match \(entry.city). User scrolled the cell out of view").ToConsole()
                   //  }
                })
            })
        }
        cell.city.text         =  entry.city +  " " + entry.country
        cell.weather.text      =  entry.weather
        cell.temperatur.text   =  entry.tempC.description + degC
        let fileName = ("\(entry.weather).gif").lowercaseString;
        if let image = UIImage(named: fileName) {
           cell.weatherImage.image     = image
        }
        return cell
    }
}