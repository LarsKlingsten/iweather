//
//  MapController.swift
//  iWeather
//
//  Created by Lars Klingsten on 26/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.

import UIKit
import MapKit
import CoreLocation

class MapController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var myLocationManager: CLLocationManager?
    
    let myData = Data.getInstance;
    var cityForecast: CityForecast!
    let degC = "\u{00B0}C"
    var myMapAnnotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLocationManager = CLLocationManager()
        myLocationManager?.delegate = self
        myLocationManager?.desiredAccuracy = kCLLocationAccuracyBest
        myLocationManager?.requestAlwaysAuthorization()
        myLocationManager?.startUpdatingLocation()
    }
    
    func setFramePosition(location: CLLocation) {
        let mapPoint = MKMapPointForCoordinate(location.coordinate)
        let mapRect = MKMapRect(origin: mapPoint, size: MKMapSize(width: 5, height: 5))
        self.mapView.setVisibleMapRect(mapRect, edgePadding: UIEdgeInsetsMake(74, 10, 10, 10), animated: false)
    }
    
    // Long Click's
    @IBAction func userSelectedLocation(sender: UILongPressGestureRecognizer) {
        
        if sender.state == UIGestureRecognizerState.Began {
            
            let point = sender.locationInView(mapView)
            let myCoordinates = mapView.convertPoint(point, toCoordinateFromView: mapView)
            
            // download weather data (via Json)
            let myJson = Json();
            
            cityForecast  = myJson.GetForecast(myCoordinates.latitude.description, longitude: myCoordinates.longitude.description)
            
            // data is avaiable
            if (cityForecast.isDataAvailable) {
                let displayFeelsLike = (cityForecast.tempC == cityForecast.feelsC) ? "" : "(feels like \(cityForecast.feelsC)\(degC)) "
                let displayThis = ("\(cityForecast.weather) @\(cityForecast.tempC)\(degC)  \(displayFeelsLike)  ")
                
                myMapAnnotation.title = cityForecast.city + " " + cityForecast.country
                myMapAnnotation.subtitle = displayThis
                myData.AddEntry(cityForecast)
                
                
                // Hack - required on the simulator to ensure that user defaults are saved.
                LoadSaveUserDefaults().saveUserDefaults()
                
            }
                // no data is available
            else {
                myMapAnnotation.title = "lat:\(myCoordinates.latitude) long: \(myCoordinates.longitude)"
                myMapAnnotation.subtitle = "sorry no data available"
            }
            
            // find and remove existing annoations
            let allAnnotations = mapView.annotations.filter { $0 !== self.mapView.userLocation }
            mapView.removeAnnotations( allAnnotations )
            
            // drop the pin and the text
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = myCoordinates
            mapView.addAnnotation(dropPin)
            myMapAnnotation.coordinate = myCoordinates
            mapView.addAnnotation(myMapAnnotation)
            mapView.selectAnnotation(myMapAnnotation , animated: true)
            
        }
    }
}

// Get your location from Iphone GPS - and turn is off again
extension MapController: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first as? CLLocation! {
            if location.horizontalAccuracy <= 20.0 {
                setFramePosition(location)
                myLocationManager?.stopUpdatingLocation()
            }
        }
    }
}