//
//  Snippets.swift
//  iWeather
//
//  Created by Lars Klingsten on 27/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import Foundation

class Snippets {
    
    class var getInstance: Snippets { // SWIFT 1.1
        struct Static {
            static let instance: Snippets = Snippets()
        }
        return Static.instance
    }
    
    private init() {
    }
    
    func stringToNSDate (dateStr: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.dateFromString(dateStr)
    }
    
    func NSDateToStringShort (date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.stringFromDate(date)
    }
    
    func NSDateToStringLong (date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.stringFromDate(date)
    }
}

extension String {
    func StringToDouble () -> Double {
        let myDouble : Double = Double((self as NSString).floatValue)
        return  round( myDouble * 100) / 100
    }
    
    func ToConsole () {
        print("\(NSDate()) \(self)" )
    }
    
    func StringToNsDate () -> NSDate?  {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.dateFromString(self)
    }
}