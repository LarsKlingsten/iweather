//
//  UserDefaults.swift
//  iWeather
//
//  Created by Lars Klingsten on 27/05/15.
//  Copyright (c) 2015 Lars Klingsten. All rights reserved.
//

import Foundation

class LoadSaveUserDefaults {
    
    let myData = Data.getInstance
    
    let country           = "country"
    let city           = "city"
    let weather         = "weather "
    let tempC           = "tempC  "
    let feelsC          = "feelsC"
    let updateWeather   = "updatedWeather"
    let updateDownload  = "updatedDownload"
    let isDataAvailable = "isDataAvailable"
    let latitude        = "latitude"
    let longitude       = "longitude"
    
    let Key = "CityForcastCollectionNew"
    
    func loadUserDefaults() {
        
        if let cityForecastCollection = NSUserDefaults.standardUserDefaults().arrayForKey(Key) as? [[String : AnyObject]] {
            
            for entry in cityForecastCollection {
                let cityForecast = CityForecast (
                    country:         entry[country] as! String,
                    city:            entry[city] as! String,
                    weather:         entry[weather] as! String,
                    tempC:           entry[tempC] as! Double,
                    feelsC:          entry[feelsC] as! Double,
                    updatedWeather:  entry[updateWeather] as! String,
                    updatedDownload: entry[updateDownload] as! NSDate,
                    isDataAvailable: entry[isDataAvailable] as! Bool,
                    latitude:        entry[latitude] as! String,
                    longitude:       entry[longitude] as! String
                )
                myData.cityForecasts.append(cityForecast)
            }
            "AppStart: loaded \(myData.cityForecasts.count) entries".ToConsole()
        }
    }
    
    
    func saveUserDefaults() {
        var userDefaultsCollection = [[String: AnyObject]]()

        for cf in myData.cityForecasts {
            var entry = [String : AnyObject]()
            
            entry[country]         = cf.country
            entry[city]            = cf.city
            entry[weather]         = cf.weather
            entry[tempC]           = cf.tempC
            entry[feelsC]          = cf.feelsC
            entry[updateWeather]   = cf.updatedWeather
            entry[updateDownload]  = cf.updatedDownload
            entry[isDataAvailable] = cf.isDataAvailable
            entry[latitude]        = cf.latitude
            entry[longitude]       = cf.longitude
            
            userDefaultsCollection.append(entry)
        }
        NSUserDefaults.standardUserDefaults().setObject(userDefaultsCollection, forKey: Key)
        "AppClosed: saved \(myData.cityForecasts.count) entries".ToConsole()
    }
}